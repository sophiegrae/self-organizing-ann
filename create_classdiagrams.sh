#!/bin/zsh
pyreverse -k som_models.py -p onlyclasses
dot -Tpng classes_onlyclasses.dot -o output_onlyclasses.png
for value in SimpleSOM GrowingSOM NumpySOM GrowingGrid GCS Neuron
do
    pyreverse -c $value -a 0 -s 0 -mn som_models.py -p $value
    dot -Tpng "$value.dot" -o "class_$value".png
done    
