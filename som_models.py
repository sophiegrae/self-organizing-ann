"""!@author Sophie Graefnitz
@date 08/2020
@brief This module contains self-organizing network implementations and related helper classes.
"""

import queue
import random
import time
import types
import sys
from operator import attrgetter
from itertools import combinations
import SixteenObjectExperimentConstants as sx_const
from sklearn.metrics import accuracy_score, log_loss
import matplotlib.pyplot as plt
import progressbar
import numpy as np

import networkx as nx
import netwulf as nw
from numpy import add, subtract, absolute, ndarray, add, nanmax, nanmin, prod, sqrt, floor, any, isnan, all,\
    isfinite, where, nanmin, delete, round, ones, int8, divide, argmax, argmin, arange, zeros
from numpy import max as npmax
from numpy import min as npmin
from numpy import sum as npsum
from numpy import exp as npexp
from numpy import array as nparray
from numpy.linalg import norm
from numpy.random import seed, uniform, choice
import evaluation as ev
import experiment as exp
import math
from itertools import product
from matplotlib.colors import to_hex


def decrease_if_bigger(array, lim):
    preserved_members = [m for m in array if m < lim]
    updated_members = [m-1 for m in array if m > lim]
    return preserved_members + updated_members


def Euclidian_distance(vec1, vec2):
    return math.sqrt(sum([(a - b)**2 for a, b in zip(vec1, vec2)]))


class SimpleSOM(object):
    """!@brief A SOM model superclass that could be instantiated. 
        A SOM which cannot grow or adapt topology anyhow, it is only able to process input data and adapt weights accordingly

        @param epochs           The number of iterations through the whole train data set (default:100)
        @param initial_nodes    A 2-tuple of dimensions set initially for the parent SOM (default: (20,20))
        @param debug            If set to True, output more information (default: False)
        @param input_dim        The input dimension of the data vectors to be processed. Set this param when the initial weight vectors should be instantiated randomly (default: None)
        @param X_validate       A validation train data set (default is empty)
        @param y_validate       The validation labels (default is empty)
        @param with_validation  Whether validation should be carried out (after each epoch, accuracy and other metrics after each epoch) (default: False)
        @param rseed            The random seed for any random generation (f.e. weight generation)(default: 12945)
        @param low              The minimum value for random generation of data points (default: -1)
        @param high             The maximum value for random generation of data points (default: 1)
        @param verbose          Whether to output verbosely (default:False)
        @param inputs           Input train data (default:empty)
        @param inputs_labels    Input train data labels (default: empty)
        @param init_from_input  Whether to pick random vectors from input data as initial weight vectors (default : False)
        @param ew               Winner weight is updated by ew * diff(winnerweight, input) (default: 0.5)
        @param en               Neighbor weight is updated by en * diff(winnerweight, input) (default:None)
        @param ew_en            The factor x when en=ew/x, default:4
        @param track_neuron_weights Whether neurons should save a weight history (default: False)
        """

    def __init__(self,  epochs=100, initial_nodes=(20, 20), sigma=2, debug=False, input_dim=None, X_validate=[], y_validate=[], with_validation=False,
                 rseed=12945, low=0, high=1, verbose=False, inputs=[], inputs_labels=[], init_from_input=False,
                 ew=0.5, en=None, ew_en=4, track_neuron_weights=False, init_model=True, **kwargs):

        super(SimpleSOM, self).__init__()
        seed(rseed)
        random.seed(rseed)
        if type(inputs) not in [list, ndarray, list]:
            raise TypeError(
                "Train input data needs to be of type numpy.ndarray or list")
        if type(inputs_labels) not in [list, ndarray, list]:
            raise TypeError(
                "Train input label data needs to be of type numpy.ndarray or list")
        if type(X_validate) not in [list, ndarray, list]:
            raise TypeError(
                "Test input data needs to be of type numpy.ndarray or list")
        if type(y_validate) not in [list, ndarray, list]:
            raise TypeError(
                "Test input label data needs to be of type numpy.ndarray or list")
        self.time = 1
        self.epoch = 1
        self.epochs = epochs
        self.label_count = None
        self.label_total = dict()
        self.default_label = None
        self.default_label_map = None
        self.winning_class_assignments = None
        self.default_winner_label_counter = 0
        self.initial_nodes = initial_nodes
        self.debug = debug
        self.verbose = verbose
        self.track_neuron_weights = track_neuron_weights
        self.low = low  # lower border for random train data generation
        self.high = high  # higher border for train data generation
        self.init_from_input = init_from_input
        self.inputs = inputs
        self.inputs_labels = inputs_labels
        self.sigma = sigma
        self.use_major_label_as_default = False
        self.repr_dict = {"model type": type(self)}
        if not init_from_input and input_dim is None and len(inputs) == 0:
            raise ValueError("Not enough information to initialize weights.")
        self.input_dim = input_dim if not input_dim is None else len(
            self.inputs[0])
        self.ew = ew  # winner update factor
        self.en = ew/ew_en if en is None else en  # neighbor update factor
        if not ((self.en <= self.ew) and (self.ew <= 1) and (self.en >= 0)):
            raise ValueError(
                "en must be less or equal than ew and must be in interval [0,1]")
        self.validate = with_validation
        self.validation_results = list()
        self.X_vld = X_validate
        self.y_vld = y_validate
        self.default_props = list()
        self._bar = progressbar.ProgressBar(maxval=self.epochs)
        self.neurons = []
        self.abort = False
        if init_model:
            self.init_model()

    def init_model(self):
        """!@brief Initialize model topology and internal attributes like a grid"""
        if not self.init_from_input and self.input_dim is None:
            raise ValueError
        self.neurons = []
        for i in range(prod(self.initial_nodes)):
            self.neurons.append(Neuron(i, self.get_network_neighbors(
                i), self.get_weight(), track_weights=self.track_neuron_weights))

    def get_shortstring(self):
        """!@brief Generates a unique identifier string for the model based on the representation dictionary. 
        @return A short string consisting of model type and the values of parameters concatenated in the order of representation dict
        @remark Be careful, in order to use this as unique identifier dont change the order of items in the representation dict."""
        shortstring = "".join([n for n in (
            filter(str.isalnum, str(self.repr_dict["model type"]).split(".")[-1]))])
        for key, value in list(self.repr_dict.items())[1:]:
            shortstring = shortstring + "_" + str(value)
        return shortstring

    def get_row_length(self):
        """!@brief Compute the row length for a grid-shaped SOM
        @return The second number of initial node tuple or sqrt(initial_nodes) if initial_nodes is an integer"""
        if type(self.initial_nodes) is tuple:
            return self.initial_nodes[1]
        else:
            return floor(sqrt(self.initial_nodes))

    def get_col_length(self):
        """!@brief Compute the column length for a grid-shaped SOM
        @return The second number of initial node tuple or sqrt(initial_nodes) if initial_nodes is an integer"""
        if type(self.initial_nodes) is tuple:
            return self.initial_nodes[0]
        else:
            return floor(sqrt(self.initial_nodes))

    def neighbour_degr(self, curr_i, curr_j, other_i, other_j):
        """!@brief Calculates the neighbor degree for units located at the given indices also considering time decay. 
        @param curr_i The row index of the unit curr
        @param curr_j The column index of the unit curr
        @param other_i The row index of another unit
        @param other_j The column index of another unit
        @param return The neighbor degree for two units"""
        return self.Gaussian(Euclidian_distance([curr_i, curr_j], [other_i, other_j]))

    def Gaussian(self, dist, sigma=None):
        """!@brief Compute the Gaussian function exp(- (dist**2)/(2 * (sigma * decay)**2)) 
        @param dist A distance (real number)
        @param decay A factor the smaller the smaller the value of the Gaussian function
        @return The Gaussian function value with the given parameters"""
        sigma = sigma if not sigma is None else self.sigma
        return npexp(- (dist**2)/(2 * (sigma * self.decay())**2))

    def get_network_neighbors(self, index):
        """!@brief Compute the neighbors of the ith neuron when the SOM has a grid shape (connected to horizontal and vertical neighbors)
        @index  The targetneuron index
        @return The neighbors of the index neuron"""
        rowl = self.get_row_length()
        coll = self.get_col_length()
        j = index % rowl  # pos
        i = floor(index/rowl)  # pos
        left = i*rowl + (j + (rowl-1)) % rowl
        right = i*rowl + (j + 1) % (rowl)
        up = ((i + coll-1) % (coll))*rowl + j
        down = ((i + 1) % (coll))*rowl + j
        res = list([left, right, up, down])
        if self.debug:
            print(("initial neighbors for " + str(index) + ": " + str(res)))
        if left > index:
            res.remove(left)
        if right < index:
            res.remove(right)
        if up > index:
            res.remove(up)
        if down < index:
            res.remove(down)
        return res

    def decay(self, exp=3):
        """!@brief Compute a decay factor based on a decay function and current epoch for the adaption rate over time 
        The function computes the decay value via total_epochs/(current_epoch^exp/total_epochs + total_epochs)
        @param exp The exponent (default:3)
        @return A decay factor """
        return self.epochs/(self.epoch**exp/self.epochs + self.epochs)

    def get_weight(self):
        """!@brief Pull a random weight from input (train) data or generate a weight depending on value of init_from_input
        @return A weight vector"""
        if self.init_from_input:
            if not any(isnan(self.inputs)):
                return self.pull_random_weight()
            else:
                raise RuntimeError
        return self.generate_random_weight()

    def generate_random_weight(self):
        """!@brief Generate a random weight from a uniform distribution in borders of size input_dim
        @return A random weight vector"""
        return uniform(self.low, self.high, self.input_dim)

    def pull_random_weight(self):
        """!@brief Pull a random vector from input data
        @return A randomly chosen weight vector"""
        if len(self.inputs) == 0:
            raise ValueError(
                "Cannot pull random weight vector from empty inputs")
        return self.inputs[random.randint(0, len(self.inputs)-1)]

    def check_network_state(self):
        """!@brief Check if the network still fulfills topological and training requirements. 
        @return False and error message if not. """
        return True, ''

    def print_weights(self):
        """!@brief Print all model weights"""
        print("Neuron weights")
        for n in self.neurons:
            print(("Neuron index: " + str(n.index)))
            print(("Neuron weight: " + str(n.weight)))

    def train(self, inputs=None):
        """!@brief Train the model with either input data given at model instantiation or provided input.
        Trains the belonging model epoch-wise and input-vector-wise if the input vector is not None. 
        Checks the network state before a new train step with an input vector if the network can still be trained. 
        After each epoch, perform eventually validation and update a model-internal progress bar. 
        At the beginning of each epoch, time is reset to 1. 
        @param inputs User provided input data (default: None)
        @remark Input data provided to this method will overwrite input data provided at object instantiation"""
        if (not any(inputs)) & (not any(self.inputs)):
            raise RuntimeError
        elif inputs != None:
            self.inputs = inputs
        if self.verbose:
            print("Enter training for the following model: ")
            print(self)
        self._bar.start()
        for epoch in range(1, self.epochs+1):
            if not self.abort:
                self.time = 1
                self.epoch = epoch
                for input_vec in self.inputs[choice(self.inputs.shape[0], self.inputs.shape[0], replace=False)]:
                    if not any(isnan(input_vec)):
                        ok, msg = self.check_network_state()
                        if not ok:
                            print(msg)
                            self.abort = True
                            break
                        if ok and len(msg) != 0:
                            print(msg)
                            self.validate_model()
                            return ok, "Training was aborted but successful."
                        ok, msg = self.train_step(input_vec)
                        if not ok:
                            print(msg)
                            self.abort = True
                            break
                        if ok and len(msg) != 0:
                            print(msg)
                            self.validate_model()
                            return ok, "Training was aborted due to legal limitations but last train step successful"
                if self.validate and not self.abort:
                    self.validate_model()
            if self.debug:
                print("")
                print(("Weights after training in epoch " + str(epoch) + "\n"))
                self.print_weights()
            if not self.abort:
                self._bar.update(self.epoch)
            else:
                return ok, "Training was aborted"
        return True, ''

    def validate_model(self):
        """!@brief Validates the current model as a classifier
        Validation can only be carried out if input labels were provided to the model. 
        Will be carried out on train data and labels and validation dataset if they were provided. 
        """
        if len(self.inputs_labels) == 0 or len(self.inputs) == 0:
            raise RuntimeError(
                "Cannot calculate a label map from data. Either data labels or data is missing.")
        self.calc_label_map(self.inputs, self.inputs_labels)
        self.validation_results.append([])
        self.validate_on_dataset(self.inputs, self.inputs_labels)
        if len(self.X_vld) != 0 and len(self.y_vld) == len(self.X_vld):
            self.validate_on_dataset(self.X_vld, self.y_vld)

    def validate_on_dataset(self, X, y_true, with_log_loss=True):
        """!@brief  Calculates metrics for the given dataset and append results to self.validation_results (hierarchy will be epoch ->dataset ->dict of metrics)
        @requires The neuron label maps are already calculated. 
        @param X The data vectors
        @param y The data vector labels
        @param with_log_loss Whether to compute the log loss (default:True)
        """
        pred, proba = self.classify(X)
        acc = accuracy_score(y_true, pred)
        s_error = sx_const.get_subj_error(pred, y_true)
        s_acc = sx_const.get_subj_accuracy(pred, y_true)
        if not all(isfinite(y_true)):
            print(y_true)
            raise ValueError("y_true contains values which are not finite.")
        if any(isnan(y_true)):
            print(y_true)
            raise ValueError("y_true contains NaN values")
        if not all([isfinite(a) for a in proba]):
            raise ValueError("probability vector contains nonfinite values")
        if any([isnan(a) for a in proba]):
            raise ValueError("probability vector contains NaN values")
        if with_log_loss:
            cross_entropy_loss = log_loss(y_true, proba)
        else:
            cross_entropy_loss = -1
        topographic_error = self.calc_topographic_error(X)
        alpha_error = self.calc_alpha_error(X)
        self.validation_results[self.epoch-1].append({"accuracy": acc,
                                                      "categorical cross-entropy loss": cross_entropy_loss,
                                                      "default proportion": self.default_props[self.epoch-1],
                                                      "topographic error": topographic_error,
                                                      "alpha error": alpha_error,
                                                      "subjective error": s_error,
                                                      "subjective acc": s_acc})

    def calc_topographic_error(self, X):
        """!@brief Calculate proportion of data points from X whose first and second best matching unit are NOT neighbors.
        @param X Input data vectors
        @return A relative topographic error to the amont of data vectors provided (length of X)
        """
        error = 0
        for x in X:
            fbmu = self.find_winner_neuron(x)
            sbmu = self.find_second_winner_neuron(x)
            if not self.are_neighbors(fbmu, sbmu):
                error += 1
        return error/len(X)

    def calc_alpha_error(self, X):
        """!@brief Calculate alpha error.
        Definition is like topographic error, but assign error 1, when nodes are no indirect neighbors and error 1/2 if they are indirect neighbors.
        @param X Input data vectors
        @return The alpha error relative to the amount of data vectors provided (length of X)
        """
        error = 0
        for x in X:
            fbmu = self.find_winner_neuron(x)
            sbmu = self.find_second_winner_neuron(x)
            if not (self.are_neighbors(fbmu, sbmu) or self.are_indirect_neighbors(fbmu, sbmu)):
                error += 1
                if self.debug:
                    print("no neighbors of any kind:", fbmu, sbmu)
            elif self.are_indirect_neighbors(fbmu, sbmu):
                error += 0.5
                if self.debug:
                    print("indirect neighbors:", fbmu, sbmu)
        return error/len(X)

    def train_step(self, input):
        """!@brief Perform a train step with one input vector and count up time. 
        @param input The input vector"""
        winner = self.find_winner_neuron(input)
        success, msg = self.adapt_step(winner, input)
        self.time += 1
        return success, msg

    def are_neighbors(self, n1, n2):
        """!@brief neighbor check for a pair of neurons. 
        @param n1 First neuron
        @param n2 Second neuron
        @return True if neurons are direct neighbors"""
        return self.neurons[n1].is_neighbor(self.neurons[n2]) and self.neurons[n2].is_neighbor(self.neurons[n1])

    def are_indirect_neighbors(self, n1, n2):
        """!@brief Indirect neighbor check for a pair of neurons.
        @param n1 First neuron
        @param n2 Second neuron
        @return True if node 1 and node 2 share one neighbor"""
        return not not set(self.neurons[n1].neighbors).intersection(set(self.neurons[n2].neighbors))

    def find_winner_neuron(self, input_vec):
        """!@brief Find The neuron whose weight vector matches the input best
        @param input_vect The input data vector"""
        inter = [n.diff_weight(input_vec) for n in self.neurons]
        return where(inter == nanmin(inter))[0][0]

    def find_second_winner_neuron(self, input_vec):
        """!@brief Find the neuron whose weight vector matches the input second best
        @param input_vec The input data vector"""
        inter = [n.diff_weight(input_vec) for n in self.neurons]
        del inter[int(where(inter == nanmin(inter))[0][0])]
        return where(inter == nanmin(inter))[0][0]

    def adapt_step(self, winner, input):
        """!@brief Adapt the model and neurons after processing one input vector
        For SimpleSOM, this will only update the neurons themselves, not their topology. 
        @param winner The index of the neuron which has won the matching for the  input vector
        @param input The input vector
        @return Any return values returned by adaption procedure"""
        return self.adapt_neurons(winner, input)

    def adapt_neurons(self, winner, input):
        """!@brief Adapt the neurons after receving input vectors.
        @param winner index of the neuron which has won the matching for the input vector
        @param input The input vector"""
        self.update_winner(winner, input)
        self.update_neighbors(winner, input)
        self.update_all_neurons()
        return True, ""

    def update_winner(self, winner_index, input):
        """!@brief Update the winner neuron attributes
        @param winner_index Index of the neuron which has won the matching for the input vector
        @param input The input vector"""
        self.update_winner_weight(winner_index, input)

    def update_winner_weight(self, winner_index, input):
        """!@brief Update the model weights for a winner neuron and the given input
        The winner neurons weight is updated by the euclidian distance to the input vector scaled by factor ew. 
        @param winner_index Index of the neuron which has won the matching for the input vector
        @param input The input vector"""
        self.neurons[winner_index].add_to_weight(
            self.ew * subtract(input, self.neurons[winner_index].weight))

    def update_all_neurons(self):
        """!@brief Update all neuron attributes after receiving one input unit"""
        pass

    def update_neighbor_weight(self, winner, input, nid):
        """!@brief Update the model weights for neighbor neurons of winner neuron and the given input
        The neighbor neurons weight is updated by the euclidian distance of winner neuron weight to the input vector scaled by factor en. 
        @param winner Index of the neuron which has won the matching for the input vector
        @param input The input vector
        @param nid The index of one of the neighbor neurons of the winner """
        self.neurons[int(nid)].add_to_weight(
            self.en * subtract(input, self.neurons[int(winner)].weight))

    def update_neighbors(self, winner, input):
        """!@brief Update all neighbor neurons of winner neuron
        @param winner index The index of the neuron which has won the matching for the input vector
        @param input The input vector"""
        for nid in self.neurons[winner].neighbors:
            self.update_neighbor_weight(winner, input, nid)

    def reset_freq_activated(self):
        """!@brief Reset frequency activated of all model neurons to 0"""
        for i in range(len(self.neurons)):
            self.neurons[i].freq_activated = 0

    def calc_edges(self):
        """!@brief Calculate the network edges as a list of tuples"""
        edges = set()
        for n in self.neurons:
            for m in n.neighbors:
                edges.add(tuple(sorted([n.index, m])))
        self.edges = list(edges)
        return list(edges)

    def update_label_map(self, winner, label):
        """!@brief Update the label map for the winner neuron
        The label map of the winner neuron will save that it has won the matching competition for an input vector of the given label.
        This is stored in a count dictionary belonging to the neuron. 
        @param winner The index of the neuron which has won a best matching neuron weight competition
        @param label The label of the input vector that the winner weight was the most similiar to
        """
        self.neurons[int(winner)].label_map[label] += 1

    def init_label_map_all_neurons(self, label_count_template):
        """!@brief Initialize the label map for each of the model's neurons
        @param label_count_template A template for counting the occurences of labels"""
        for i in self.neurons:
            i.label_map = label_count_template.copy()

    def calc_label_map(self, inputs, labels):
        """!@brief Calculate the label map of each model neuron
        Before, all label maps and winner labels are reset for all neurons
        Initialize the empty label maps for all model neurons. 
        Go through inputs and labels and record which neuron has won the weight matching by counting up the entry in the neuron's label map. 
        Calculate default labels and label maps for neurons which have never won anything. 
        @post all neurons which have ever won a best matching competition have assigned a label map.  
        @param inputs the input vectors to calculate the neuron label maps with
        @param labels the labels belonging to the input data. The input data labels should be the same as the model was trained with.  """
        if len(inputs) != len(labels):
            raise ValueError(
                'Input data amount needs to equal number of labels')
        # would in theory also work for non-integer labels
        distinct_labels = set(labels)
        label_count_template = {}
        for label in distinct_labels:
            label_count_template[label] = 0
        self.init_label_map_all_neurons(label_count_template)
        self.default_label = None
        self.winning_class_assignments = None
        #print(("Number of labels is " + str(len(distinct_labels))))
        for _, (x, y) in enumerate(zip(inputs, labels)):
            winner = self.find_winner_neuron(x)
            self.update_label_map(winner, y)
        self.label_count = len(distinct_labels)
        self.distinct_labels = distinct_labels
        self.calc_default_label()
        self.calc_default_label_map()

    def calc_winning_class_assignments(self):
        """!@brief Calculate the winning class assignment for each neuron and set results as winning_class_assignements.
        The winning class assignment is based on the label map computed for each neuron. 
        The winning class for a neuron is the label that occurred most, namely the label with the most counts in a neuron label map. 
        When a neuron has no entries in its label map, the default label is assigned and a default label map is assigned. 
        Each time a neuron is assigned default label and label map, a counter is incremented. 
        The percentage of neurons which have default assignments is added to the self.default_props history used in validation. 
        @pre The label map for each neuron needs to be computed already.
        @return the winning_class_assignments dict (neuron index to label)
        """
        winning_class_assignments = {}
        self.default_winner_label_counter = 0
        for n in self.neurons:
            if round(sum(n.label_map.values()), 4) == 0.0:
                self.set_default_label(n.index)
                self.set_default_label_map((n.index))
                self.default_winner_label_counter += 1

            else:
                n.winner_label = sorted(
                    list(n.label_map.items()), key=lambda x: x[1], reverse=True)[0][0]
            winning_class_assignments[n.index] = n.winner_label
        default_prop = self.default_winner_label_counter/len(self.neurons)*100
        self.default_props.append(default_prop)
        if self.verbose:
            print("Set default neuron values in", default_prop, "% of cases.")
        self.winning_class_assignments = winning_class_assignments
        return winning_class_assignments

    def calc_default_label(self):
        """!@brief Calculate the default label for a neuron based on all neuron label maps.
        If this model should use a neutral label instead, set the neutral label as the default label. 
        The label which occurred the most often in label maps regarding all neurons will be set as default neuron."""
        if self.use_major_label_as_default:
            self.label_total = dict.fromkeys(self.distinct_labels, 0)
            for n in self.neurons:
                for key in list(n.label_map.keys()):
                    self.label_total[key] += n.label_map[key]
            self.default_label = sorted(list(self.label_total.items()),
                                        key=lambda x: x[1], reverse=True)[0][0]
        else:
            self.default_label = max(self.distinct_labels)+1

    def calc_default_label_map(self):
        """!@brief Calculate default label map as an equal distribution over all labels and set it as a model field. """
        self.default_label_map = dict(
            zip(self.distinct_labels, list(ones(self.label_count, dtype=int8))))

    def set_default_label(self, index):
        """!@brief Set default label for the neuron with the index
        @param index the neuron index
        @pre The default label needs to be computed."""
        self.neurons[index].winner_label = self.default_label

    def set_default_label_map(self, index):
        """!@brief Set the default label map for neuron at index index
        @pre The default label map needs to be computed. """
        self.neurons[index].label_map = self.default_label_map

    def classify(self, inputs):
        """!@brief classify the given inputs based on prior training.
        @pre The neurons need to have a computed label map. 
        @param inputs The input data which will be classified
        @return  A list of winner labels and a list of class probablity vectors"""
        pred = []
        proba = []
        if self.winning_class_assignments is None:
            self.calc_winning_class_assignments()
        for input in inputs:
            winner = self.find_winner_neuron(input)
            if self.get_winner_label_for_index(winner) is None:
                raise RuntimeWarning("Needed to assign default label map and value\
                for neuron although winning class assignments were already calculated. Error in your implementation")
                self.set_default_label(winner)
                self.set_default_label_map((n.index))
            proba.append(self.get_class_probabilities_for_index(winner))
            pred.append(self.get_winner_label_for_index(winner))
        return pred, proba

    def gather_node_positions(self):
        """!@brief Computes initial grid positions for all neurons and saves them in res
        @remark the node indices do not need to be sequential. 
        @return a dictionary of neuron indices to grid position tuple"""
        nodes = {}
        turn = floor(sqrt(len(self.neurons)))
        counter = 0  # in case ids are not sequential
        for n in self.neurons:
            nodes[n.index] = (floor(counter/turn), counter % turn)
            counter += 1
        self.nd_pos_dict = nodes
        return nodes

    def get_winner_label_for_index(self, index):
        """!@return The winner label for the neuron at index index"""
        return self.neurons[index].winner_label

    def get_label_map_for_index(self, index):
        """!@return The label map for the node number index"""
        return self.neurons[index].label_map

    def get_class_probabilities_for_index(self, index):
        """!@return The label map of a neuron as probability vector"""
        total_count = list(self.get_label_map_for_index(index).values())
        total = sum(total_count)
        if round(total, decimals=4) == 0:
            return ones(len(total_count))/len(total_count)
        else:
            return divide(total_count, total, where=total != 0)

    def get_labels_index_for_nodes(self, G):
        labels = {}
        node_to_index = {}
        for n in G.nodes():
            labels[n] = int(self.get_winner_label_for_index(n))
            node_to_index[n] = n
            if labels[n] is None:
                labels[n] = int(self.default_label)
        return labels, node_to_index

    def create_graph(self, disconnected=True):
        """!@brief Creates a networkx graph from the model topology. 
        Creates a networkx graph from the list of neurons and their guessed grid positions. 
        For the case that no winner label is computed for a neuron yet, the default label and (and hence default color) is set for this neuron. 
        @param disconnected whether to separate connected components
        @return the Graph object, the labels assigned to the graph-nodes, a dictionary mapping Graph node to neuron index"""
        G = nx.Graph()
        # corresponds to former neuron indices
        G.add_nodes_from(list(self.gather_node_positions().keys()))
        G.add_edges_from(self.calc_edges())
        subgraphs = list()
        if disconnected and not nx.is_connected(G):
            G_comp = nx.connected_components(G)
            for comp in G_comp:
                G = nx.Graph()
                G.add_nodes_from(comp)
                comp_edges = list()
                for f, s in self.calc_edges():
                    if f in comp and s in comp:
                        comp_edges.append((f, s))
                G.add_edges_from(comp_edges)
                labels, node_to_index = self.get_labels_index_for_nodes(G)
                if len(G.nodes()) > 0:
                    subgraphs.append((G, labels, node_to_index))
        else:
            labels, node_to_index = self.get_labels_index_for_nodes(G)
            subgraphs.append((G, labels, node_to_index))
        return subgraphs

    def get_figsize_for_nodes(self, number_nodes):
        if number_nodes < 100:
            return (10, 10)
        elif number_nodes > 300:
            return (20, 20)
        else:
            return (int(number_nodes*0.7/10), int(number_nodes*0.7/10))

    def plot_graph(self, option="kamada-kawai", linewidths=0.1, node_size=650, separate_components=False,
                   width=0.1, edge_color="black", node_annot=None, font_color="white",
                   cmap=sx_const.mycolormap, font_size=14, show_node_indices=False, show_node_labels=False):
        """!@brief Plot the model as a graph, default layout is kamada-kawai. 
        @param option       The networkx graph layout (default: kamada-kawai).
        @param linewidths    The networkx edge linewidths (default: 0.1). 
        @param node_size    The networkx node size (default: 3). 
        @param width        The networkx width (of node surrounding line)(default: 0.01). 
        @param edge_color   The networkx colour of edges (default: black). 
        @param cmap         The colormap used to colour the networkx graph nodes according to their labels. 
        @param font_size    If set to other than default (None), annotate the graph with font with this fontsize. 
        @param font_color   If set to other than default (None), use this font color for annotations (defaults to black)
        @param show_node_indices Whether to annotate the graph nodes with the neuron indices.
        @param show_node_labels Whether to annote the graph nodes with the neuron winner labels.
        @return The matplotlib.pyplot figure showing the networkx graph. """
        if show_node_indices and show_node_labels:
            return ValueError("Cannot show both: neuron index and winner label value")
        figs = list()
        for G, node_to_label, node_to_index in self.create_graph(disconnected=separate_components):
            if show_node_indices:
                node_annot = node_to_index
            if show_node_labels:
                node_annot = node_to_label
            fig_size = self.get_figsize_for_nodes(len(G.nodes))
            fig = plt.figure(figsize=fig_size)
            if (len(cmap.colors) > len(set(node_to_label.values()))) and (max(node_to_label.values()) <= len(cmap.colors)):
                #provide a list of colors if cmap dimension does not match number of labels
                node_color = [cmap.colors[i] for i in node_to_label.values()]
            elif len(cmap.colors) < len(set(node_to_label.values())):
                raise RuntimeWarning("Colormap not large enough to display all labels with individual color")
            else:
                node_color = list(node_to_label.values())
            if option == "kamada-kawai":
                nx.draw_kamada_kawai(G, linewidths=linewidths, node_size=node_size, width=width, font_color=font_color,
                                     edge_color=edge_color, node_color=node_color, labels=node_annot, cmap=cmap, font_size=font_size)
            elif option == "circular":
                nx.draw_circular(G, linewidths=linewidths, node_size=node_size, width=width, font_color=font_color,
                                 edge_color=edge_color, node_color=node_color, labels=node_annot, cmap=cmap, font_size=font_size)
            elif option == "shell":
                nx.draw_shell(G, linewidths=linewidths, node_size=node_size, width=width, font_color=font_color,
                              edge_color=edge_color, node_color=node_color, cmap=cmap, labels=node_annot, font_size=font_size)
            elif option == "spectral":
                nx.draw_spectral(G, linewidths=linewidths, node_size=node_size, width=width, font_color=font_color,
                                 edge_color=edge_color, node_color=node_color, labels=node_annot, cmap=cmap, font_size=font_size)
            elif option == "spring":
                nx.draw_spring(G, linewidths=linewidths, node_size=node_size, width=width, font_color=font_color,
                               edge_color=edge_color, node_color=node_color, labels=node_annot, cmap=cmap, font_size=font_size)
            else:
                if option != "simple":
                    print(Warning(
                        "The option you specified is not recognized. Will use the default draw algorithm."))
                nx.draw(G, linewidths=linewidths, node_size=node_size, width=width, font_color=font_color,
                        edge_color=edge_color, node_color=node_color, labels=node_annot, cmap=cmap, font_size=font_size)
            plt.show()
            figs.append(fig)
        return figs

    def get_strengthed_samelabel_edges(self, G):
        """!@brief Computes thicker edges for those graph nodes which have the same label (aka group)
        @param G    A networkx graph
        @return     A dictionary of edges to edge weights"""
        edge_weights = dict()
        for e in G.edges:
            if G.nodes[e[0]]['group'] == G.nodes[e[1]]['group']:
                edge_weights[e] = 100
            else:
                edge_weights[e] = 1
        return edge_weights

    def create_graph_interactive(self, cmap=sx_const.mycolormap):
        """!@brief Creates a graph from this model which can be visualized with netwulf. 
        The graph nodes will be coloured according to their label and have thicker edges between nodes with the same label. 
        @param cmap the colormap that is used to map node labels to colours. 
        @return the networkx Graph object, the label to hexadecimal-encoded colour dictionary"""
        G, labels, _ = self.create_graph(disconnected=False)[0]
        # netwulf needs hexadecimal colors
        labels = dict([k, to_hex(cmap.colors[v])]for k, v in labels.items())
        nx.set_node_attributes(G, values=labels, name='group')
        edge_weights = self.get_strengthed_samelabel_edges(G)
        nx.set_edge_attributes(G, values=edge_weights, name='weight')
        return G, labels

    def add_samelabel_edges(self, G, labels):
        """!@brief Adds all pairwise edges between nodes with same label. 
        Weak weight for new edges, the old ones are untouched. 
        This method is meant to visualize groups of nodes with same labels closer to each other
        which might not be connected with each other actually.
        @param G        The networkx graph object
        @param labels   A dictionary of node indices to assigned labels
        @return         the modified graph object"""
        label2nodeid = {n: [k for k in labels.keys() if labels[k] == n]
                        for n in set(labels.values())}
        for k in label2nodeid.keys():
            labelcluster_edges = set([(i, v) for (i, v) in combinations(
                label2nodeid[k], 2)]).difference(set(G.edges))
            G.add_weighted_edges_from([(i, v, 0.01)
                                       for (i, v) in labelcluster_edges])

    def plot_graph_interactive(self, port=8881, cmap=sx_const.mycolormap):
        """!@brief Plots the network interactively with netwulf library.
        The graph is created with method create_graph_interactive. 
        @param port     The port of localhost which will be exposed for the interactive visualization. 
        @param cmap     The colormap which is used to colour the nodes according to their label
        @return         the netwulf object for visualization. """
        G, _ = self.create_graph_interactive(cmap=cmap)
        return nw.visualize(G, port=port)

    def plot_graph_grouped_interactive(self, port=8881):
        """!@brief Plots the network interactively with netwulf library. 
        Adds weak artificial edges between nodes with same label to pull them together. 
        Nodes with same values are stronger connected if there are already edges
        The graph is created with method create_graph_interactive. 
        @param port     The port of localhost which will be exposed for the interactive visualization. 
        @param cmap     The colormap which is used to colour the nodes according to their label
        @return         the netwulf object for visualization."""
        G, labels = self.create_graph_interactive()
        self.add_samelabel_edges(G, labels)
        return nw.visualize(G, port=port)

    def restore_model_defaults(self):
        """!@brief Restore all defaults specific to this model"""
        self.time = 1
        self.epoch = 1
        self.validation_results = list()
        self._bar = progressbar.ProgressBar(maxval=self.epochs)
        self.label_count = None
        self.label_total = dict()
        self.default_label = None
        self.default_label_map = None
        self.winning_class_assignments = None
        self.default_winner_label_counter = 0
        self.abort = False

    def reinitialize(self, X_train, y_train, X_test, y_test):
        """!@brief Reinitialize the model with the given datasets
        @param X_train The train data set
        @param y_train The train data class labels
        @param X_test The test data set
        @param y_test The test data class labels"""
        # general default values
        self.restore_model_defaults()
        self.inputs = X_train
        self.inputs_labels = y_train
        self.X_vld = X_test
        self.y_vld = y_test
        self.init_model()

    def train_evaluate(self, X_train, y_train, X_test, y_test, cmap=sx_const.mycolormap, plot_class_proba=False, plot_validation=True, path=".", prefix="", class_names=None):
        """!@brief Train with the given test and train data set and perform validations afterwards, using the given colormap for labels. 
        @param X_train The train data set
        @param y_train The train data class labels
        @param X_test The test data set
        @param y_test The test data class labels
        @param cmap The colormap used to color the labels in the plots
        @param plot_class_proba Whether to create plots to analyze the class probabilities with which the classes were assigned (default: False)
        @param plot_validation Whether to create plots showing the model network and performance evaluation graphs (default:True)
        @param path The path where The images shall be stored
        @param prefix The prefix of the filenames of the images
        @return pred The predicted classes for X_test
        @return proba The class probability vectors for inputs from X_test
        @return figures The evaluation and probability vector plots which were eventually created"""
        self.reinitialize(X_train, y_train, X_test, y_test)
        num_classes=len(set(y_train).union(set(y_test)))
        if class_names is None: 
                class_names = [str(i) for i in range(num_classes)]
        ok, msg = self.train()
        figures = list()
        if not ok:
            print(msg)
            return [], [], figures
        _ = self.calc_label_map(X_train, y_train)
        pred, proba = self.classify(X_test)
        if plot_validation:
            acc = accuracy_score(y_test, pred)
            print("Accuracy: ", acc)
            print("Subjective Accuracy: ",
                  sx_const.get_subj_accuracy(y_test, pred))
            print("Subjective Error: ", sx_const.get_subj_error(y_test, pred))
            figures.append(ev.cm_analysis(y_test, pred, labels=range(num_classes),
                                          ymap=dict(zip(range(num_classes+1), class_names+["default"]))))
            if hasattr(self, 'delete_neurons') and self.delete_neurons:
                display_option = "spring"  # better display of disconnected components
            else:
                display_option = "kamada-kawai"
            [figures.append(f) for f in self.plot_graph(
                show_node_indices=True, separate_components=False, option=display_option)]
            figures.append(ev.plot_validation_results(self, cmap))
            exp.savefigs(figures, path,
                         prefix+self.get_shortstring()+"_acc_" + str(round(acc, decimals=4)))
            plt.close("all")
        if plot_class_proba:
            figures.append(
                ev.plot_class_probabilities_per_label_stacked(proba, pred, cmap, num_classes=num_classes, class_names=class_names))
        return pred, proba, figures


##############################################################################################
##############################################################################################

class GrowingSOM(SimpleSOM):
    """!@brief A growing cell model abstract superclass for all growing cell structures or self organizing maps from this package. 
    @details Models implementing this class may either grow (and shrink) until the maximum epoch was reached,or the training can stop when the maximum size was reached, 
    or you can set one of use_fine_tune_iterations or use_fine_tune_epochs to True and let the model train on for max_fine_tune_units iterations or epochs.  
    When both units and epochs are used to count the fine tune units, actually the units are applied. 
    @param max_size         A tuple of integers/integer indicating the grid size/total number of neurons which amay not be exceed in either dimension (default: (15,15)). 
    @param l                Each lambda (positive integer) input vectors processed eventually a growth step is performed if all other conditions are true (default: 10).
    @param delete_neurons   Whether to delete neurons when maximum size is reached (default: False). 
    @param alpha            A factor between 0 and 1 regulating the adaption strength of neuron local errors (default: 1).
    @param max_fine_tune_iters The number of fine tune iterations after the maximum size was reached the first time (default: 5).
    @param max_fine_tune_epochs The number of finetune epochs after the maximum size was reached the first time (default: 5).
    @param use_fine_tune_iterations Whether to use iterations after the maximum size was reached to finetune the model (default: False). 
    @param use_fine_tune_epochs     Whether to use epochs after the maximum size was reached to finetune the model (default: False). 
    @param stop_at_max_nodes        Whether to stop the training immediately when the maximum size was exceeded. Do not set to True when fine tune units are wanted (default: False)."""

    def __init__(self, max_size=(15, 15), l=10, delete_neurons=False, alpha=1, max_fine_tune_iters=5, max_fine_tune_epochs=5,
                 use_fine_tune_iterations=False, use_fine_tune_epochs=False, stop_at_max_nodes=False, **kwargs):
        self.max_reached = False
        self.stop_at_max_nodes = stop_at_max_nodes
        self.use_fine_tune_iterations = use_fine_tune_iterations
        self.max_fine_tune_iters = max_fine_tune_iters
        self.use_fine_tune_epochs = use_fine_tune_epochs
        self.max_fine_tune_epochs = max_fine_tune_epochs
        self.first_max_reached_epoch = None
        self.fine_tune_iterations = 0
        self.delete_neurons = delete_neurons
        self.max_size = max_size
        self.alpha = alpha
        self._lambda = l
        super(GrowingSOM, self).__init__(**kwargs)

    def restore_model_defaults(self):
        """!@brief Restore all defaults specific to this model"""
        super(GrowingSOM, self).restore_model_defaults()
        self.max_reached = False
        self.first_max_reached_epoch = None
        self.fine_tune_iterations = 0

    def grow_step(self, winner, input):
        """!@brief Grow by one unit, may be one more neuron or other param. This is an abstract method. 
        @param winner The index of the neuron which was the best matching unit for the input vector
        @param input The corresponding input vector"""
        return True, ""

    def shrink_step(self):
        """!@brief Shrink by one unit, f.e. one neuron with affected simplices"""
        return True, ""

    def adapt_step(self, winner, input):
        """!@brief Adapt the model topology and neurons after processing one input vector.
        @param winner Index of the neuron which has won the matching for the input vector
        @param input The input vector
        @return Any return values returned by adaption procedure"""
        self.adapt_neurons(winner, input)
        return self.adapt_topology(winner, input)

    def make_neighbors(self, id1, id2):
        """!@brief connects neuron with id1 to neuron with id2 and vice versa
        @param id1 The index of the first neuron
        @param id2 The index of the second neuron"""
        self.neurons[id1].add_neighbor(id2)
        self.neurons[id2].add_neighbor(id1)

    def check_less_max_size(self):
        """!@brief Check if the current amount of neurons is less than the maximum size.
        The maximum size is reached when the product of maximum size is exceeded. 
        @return True if max size is not reached yet"""
        return len(self.neurons) < prod(self.max_size)

    def check_network_state(self):
        """!@brief Checks if the network is allowed to train further considering the limits to growth and eventually fine tune units. 
        Returns True and no error message if maximum size is not exceeded and conditions are ok. Otherwise will return False and an error message which should be interpreted to stop training.
        @return OK Status (boolean), cause message """
        if not self.check_less_max_size():
            if self.stop_at_max_nodes:
                return True, "End training in epoch " + str(self.epoch) + " and time " + str(self.time) + " because maximum grid size was reached."
            self.max_reached = True
            if self.first_max_reached_epoch is None:
                if self.verbose:
                    print("reached maximum size first time in epoch ",
                          str(self.epoch))
                self.first_max_reached_epoch = self.epoch
        if not self.first_max_reached_epoch is None and self.use_fine_tune_epochs:
            if subtract(self.epoch, self.first_max_reached_epoch) >= self.max_fine_tune_epochs:
                return True, "End training in epoch " + str(self.epoch) + " and time " + str(self.time) + " because maximum fine tune epochs was reached."
        if len(self.neurons) == 0:
            return False, "Network was completely deleted by algorithm."
        return True, ''

    def check_growable(self):
        """!@return True when network can technically grow because model topology allows it (independent of max size)"""
        return True

    def find_maxval_neuron(self, attribute, highest):
        """!@brief Finds the neuron with the highest/lowest value for some neuron attribute. 
        @param attribute    Any numeric attribute of neuron object. 
        @param highest      If True, find the neuron with the maximum attribute value. Otherwise, it will be selected by a minimum value. 
        @return The Neuron object which was found to have the desired attribute property. """
        if highest:
            return max(self.neurons, key=attrgetter(attribute))
        return min(self.neurons, key=attrgetter(attribute))

    def find_maxweightdiff_neighbor(self, index):
        """!@brief Finds the neighbor of neuron with <index> with the maximum different weight from itself. 
        @param index The index of the neuron for which the neighbor is searched. 
        @return The index of this neuron"""
        neighbors = list(self.neurons[index].neighbors.copy())
        inter = npsum(absolute(subtract(self.neurons[index].weight,
                                        [self.neurons[int(n)].weight for n in neighbors])), axis=1)
        return neighbors[where(inter == nanmax(inter))[0][0]]

    def check_lambda_condition(self):
        """!@return True if the number of received input vectors (=self.time) is an integer multple of lambda"""
        return (self.time % self._lambda == 0) & (self.time > 0)

    def adapt_topology(self, winner, input):
        """!@brief Adapt model topology
        @param winner The index of the winner neuron for the input vector. 
        @param input The corresponding input"""
        if self.check_lambda_condition():
            if not self.max_reached:  # max_reached is set by check_network_state method
                if not self.check_growable():
                    return False, "The network cannot grow anymore although maximum size was not reached."
                return self.grow_step(winner, input)
            if self.max_reached:
                if self.delete_neurons:
                    self.shrink_step()
                    if not self.check_growable():
                        return False, "The network cannot grow anymore after a shrink step."
                    self.grow_step(winner, input)
                    if self.check_less_max_size():
                        self.max_reached = False
                # this does not equal a maximum number of fine tune iterations
                self.fine_tune_iterations += 1
                if self.use_fine_tune_iterations and self.fine_tune_iterations >= self.max_fine_tune_iters:
                    return True, "Maximum number of fine tune iterations reached"
        return True, ""


##############################################################################################
##############################################################################################

class Neuron(object):
    """!@brief A class representing a neuron or node of self organizing networks and maps. 
    The neuron holds several attributes, especially an index, a set of neighbors and a weight vector, as well as some additional properties necessary for the implementation. 
    @param index the identifier of this neuron. Should be uniquely assigned to each of the neurons. 
    @param neighbors a set of neuron ids which are the connected to this neuron
    @param label_map Later, when the neuron was trained, with a labelled data set a label map (counting how often the neuron has won a label) is computed. 
    @param winner_label The label that was most times won will be assigned the winner label, means when an input vector is won by this neuron, it is assigned this winner level. 
    @param freq_activated a counter how often this neuron was activated during training (activated means, that the weight vector was most similiar to a training input vector)
    @param lerror the local error tracking the difference between won input vector and neuron weight across multiple iterations (is regularly reset) (default:0)
    @param weight the weight vector of this neuron
    @param simplices a set of simplex ids to which this neuron belongs to (only needed for GrowingCellStructure) (default:empty)
    @param pos a position tuple indicating the indexed grid or network position of this neuron (default:empty)
    @param track_weights whether to keep a history of the weight vectors (default:false)
    @param epoch the epoch when this neuron was created (default:None)
    """

    def __init__(self, index, neighbors, weight, simplices=[], position=(), freq_activated=0,
                 lerror=0, track_weights=False, epoch=None):
        super(Neuron, self).__init__()
        self.index = index
        self.neighbors = set(neighbors)
        self.label_map = dict()
        self.winner_label = None
        self.freq_activated = freq_activated
        self.lerror = lerror
        self.weight = weight
        self.simplices = set(simplices)
        self.pos = list(position)
        self.track_weights = track_weights
        self.living_since_epoch = epoch
        if self.track_weights:
            self.weight_history = [list(weight).copy()]

    def decr_lerror(self, beta, r=1):
        """!@brief  Subtracts beta/r times local error from local error
        @param beta The counter 
        @param r The denominator for subtraction (default: 1)"""
        self.lerror -= (beta * self.lerror)/r

    def decr_freq(self, beta, r=1):
        """!@brief  Subtracts beta/r times freq_activated from freq_activated
        @param beta The counter 
        @param r The denominator for subtraction (default: 1)"""
        self.freq_activated -= (beta * self.freq_activated)/r

    def add_neighbor(self, new_neighbor):
        """!@brief Adds new neighbor to the existing set of neighbors
        @param new_neighbor The index of the new neighbor neuron
        """
        self.neighbors.add(new_neighbor)

    def diff_weight(self, input):
        """!@brief Calculates the euclidian distance between the given input and own weight vector
        @pre input vector needs to have same dimension as weight vector and the neuron weight needs to be initialized
        @param input the input vector to compare to the neuron weight vector
        @return the euclidian distance between input and weight vector"""
        if not any(self.weight):
            raise ValueError
        return norm(subtract(self.weight, input))

    def is_neighbor(self, other):
        """!@brief Checks whether this neuron is a neighbor of the other neuron.
        @param other the other neuron (index)
        @return True if this neuron is a neighbor of neuron other"""
        return self.index in other.neighbors

    def add_to_weight(self, summand):
        """!@brief Adds summand to current weight and appends a new entry to neuron weight history if track_weights is activated
        @param summand the summand to add"""
        if len(self.weight) != len(summand):
            raise ValueError(
                "trying to add a summand of different shape to a weight")
        self.weight = add(self.weight, summand)
        np.clip(self.weight, 0, 1, out=self.weight)
        if self.track_weights:
            self.weight_history.append(self.weight.copy())

    def plot_weight_history(self):
        """!@brief Plots the neuron weight history in a matplotlib.pyplot plot"""
        if not self.track_weights:
            raise RuntimeError("This neuron`s weights are not trackable")
        weight_dim = len(self.weight)
        # one curve per weight dimension
        for i in range(weight_dim):
            # get overall difference of weight rounded to three decimals
            diff = round(self.weight_history[len(
                self.weight_history)-1][i] - self.weight_history[0][i], 5)
            plt.plot(arange(len(self.weight_history)),
                     # get the weight history of the ith weight dimension
                     [self.weight_history[update][i]
                         for update in range(len(self.weight_history))],
                     label=str(i)+": " + str(diff),
                     rasterized=True)
        plt.legend(bbox_to_anchor=(1.0, 1.05))
        plt.xlabel("updates")
        plt.title("Weight history of Neuron " + str(self.index) +
                  " (born epoch " + str(self.living_since_epoch) + ")")
        plt.show()

    def __repr__(self):
        return str({
            'type': type(self),
            'index': self.index,
            'neighbors': self.neighbors,
            'label map': self.label_map,
            'winner label': self.winner_label,
            'frequency activated': self.freq_activated,
            'local error': self.lerror,
            'weight': self.weight,
            'simplices': self.simplices,
            'pos': self.pos
        })

##############################################################################################


class GrowingGrid(GrowingSOM):
    """!@brief A class implementing the growing grid from Johnsson et al.: Haptic Perception with Self Organizing ANNs and an Anthropomorphic Hand.
    @details Class instances begin represent a self organizing map in grid shape, which is able to grow in length and width.
    Implements GrowingSOM abstract class and inherits growing procedures from it. 
    When a neuron's neighbor degree is greater than this number (strong neighborhood), the neuron weight will be adapted to the input, 
    otherwise the distance between the two neurons will be increased (default: 0)
    @param i The number of rows (the length of columns) (default: 2)
    @param j The number of columns (the length of rows) (default:2)
    
    """

    def __init__(self, i=2, j=2,  **kwargs):
        self.i = i
        self.j = j
        self.delete_neurons = False
        super(GrowingGrid, self).__init__(**kwargs)
        if self.alpha < 0 or self.alpha > 1:
            raise ValueError("Adaption rate need to be in interval [0,1]")
        self.repr_dict = {
            'model type': type(self),
            'max size': self.max_size,
            'max fine tune iters': self.max_fine_tune_iters,
            'max fine tune epochs': self.max_fine_tune_epochs,
            'col length': self.get_col_length(),
            'row length': self.get_row_length(),
            'max epochs': self.epochs,
            'init from input data': self.init_from_input,
            'alpha (growth phase adaption rate)': self.alpha,
            'lambda': self._lambda,
            'with validation': self.validate,
            'debug mode': self.debug
        }

    def restore_model_defaults(self):
        """!@brief restore all defaults specific to this model"""
        super(GrowingGrid, self).restore_model_defaults()

    def init_model(self):
        """!@brief Initiates the growing grid model. All internal parameters need to be set before."""
        self.neurons = []
        for idx in range(self.i):
            for jdx in range(self.j):
                index = idx*self.j+jdx
                self.neurons.append(Neuron(index, neighbors=self.get_network_neighbors(index),
                                           weight=self.get_weight(), position=[idx, jdx], track_weights=self.track_neuron_weights,
                                           epoch=self.epoch))

    def __repr__(self):
        return str(self.repr_dict)

    def get_col_length(self):
        """!@brief Get the column dimension of the growing grid. 
        @return The column length"""
        return self.j

    def get_row_length(self):
        """!@brief Get the row dimension of the growing grid. 
        @return The row length"""
        return self.i

    def check_less_max_size(self):
        """!@brief Checks whether the grid dimensions do not exceed the maximum size.
        The maximum size can be defined as integer or tuple. 
        In case the maximum size is a tuple, only return true if both grid dimensions are smaller. 
        Otherwise, check if the product of grid dimensions is less than maxiumum size. 
        @return Whether the grid dimensions are below maximum size"""
        if type(self.max_size) is tuple:
            return (self.get_col_length() < self.max_size[0]) and (self.get_row_length() < self.max_size[1])
        return prod((self.get_col_length(), self.get_row_length())) < prod(self.max_size)

    def check_lambda_condition(self):
        """!@brief Checks the lambda condition.
        A new row or column is inserted if the number of input vectors 
        is an integer multiple of lambda of the current number of neurons in the grid 
        (see Johnsson et al.: Haptic Perception with Self Organizing ANNs and an Anthropomorphic Hand)
        @return Whether the passed time (=numbers of input vectors processed) is a multiple of lambda and the number of neurons"""
        return self.time % (self._lambda*len(self.neurons)) == 0

    def adjust_i_j(self, i, j):
        """!@brief resets i and j (the grid dimensions)
        @param i New column dimension
        @param j New row dimension"""
        self.i = i
        self.j = j

    def grow_step(self, winner=None, input=None):
        """!@brief Initiates the growth of either a new row or column"""
        self.insert_row_or_column()
        self.reset_freq_activated()
        return True, ""

    def insert_row_or_column(self):
        """!@brief Inserts either a new row or column. 
        The new row or column is inserted between the neuron with the maximum frequency activated 
        and its neighbor which has the maximum different weight.
        For details of insertion, please consult the description by Johnsson et al. ."""
        if len(self.neurons) == 0:
            print("Model has no neurons! ")
            print(self)
            print(self.neurons)
        q = int(self.find_maxval_neuron("freq_activated", highest=True).index)
        f = int(self.find_maxweightdiff_neighbor(q))
        if self.neurons[q].pos[0] == self.neurons[f].pos[0]:
            if self.neurons[q].pos[1] != self.neurons[f].pos[1]:
                # insert new column and return new shape
                self.insert_column(
                    self.neurons[q].pos[1], self.neurons[f].pos[1], q, f)
                self.adjust_i_j(self.i, self.j+1)
                if self.verbose:
                    print(("added column. shape is now" + str((self.i, self.j))))
            else:
                if self.verbose:
                    print(
                        "Trying to insert a new column between neurons with same positions: ")
                    print(("q: " + str(self.neurons[q])))
                    print(("f: " + str(self.neurons[f])))
                sys.exit()
        else:
            if self.neurons[q].pos[1] == self.neurons[f].pos[1]:
                if self.neurons[q].pos[0] != self.neurons[f].pos[0]:
                    # insert new row and return new shape
                    self.insert_row(
                        self.neurons[q].pos[0], self.neurons[f].pos[0], q, f)
                    self.adjust_i_j(self.i + 1, self.j)
                    if self.verbose:
                        print(("added row. shape is now" + str((self.i, self.j))))
                else:
                    if self.verbose:
                        print(
                            "Trying to insert a new row between neurons with same positions")
                        print(("q: " + str(self.neurons[q])))
                        print(("f: " + str(self.neurons[f])))
                    sys.exit()


    def insert_column(self, j1, j2, q, f):
        """!@brief Inserts a column between the j-values j1 and j2 belonging to neuron 
        indices q and f which are in same row. 
        @param j1 The first column index
        @param j2 The second column index
        @param q The neuron which has the first column index
        @param f The neuron of q which is in the same row as q and has maximum different weight from q."""
        leftj = min((j1, j2))
        rightj = max((j1, j2))
        # get if q or f is left node and find all column members by using neighbor lists
        if self.neurons[q].pos[1] == leftj:
            left_node = q
            right_node = f
        else:
            left_node = f
            right_node = q
        left_node_ids = self.find_node_column(left_node)
        right_node_ids = self.find_node_column(right_node)
        self.shift_pos_right(right_node_ids)
        self.remove_row_col_neighbor_connections(left_node_ids, right_node_ids)
        self.add_col_row_between(left_node_ids, right_node_ids, col=True)

    def insert_row(self, i1, i2, q, f):
        """!@brief Inserts a row between the i-values i1 and i2 belonging to neuron 
        indices q and f which are in same column. 
        @param i1 The first row index
        @param j2 The second row index
        @param q The neuron which has the first row index
        @param f The neuron of q which is in the same column as q and has maximum different weight from q."""
        # get if q or f is upper node and find all row members by using neighbor lists
        upper_i = min((i1, i2))
        lower_i = max((i1, i2))
        if self.neurons[q].pos[0] == upper_i:
            upper_node = q
            lower_node = f
        else:
            upper_node = f
            lower_node = q
        up_node_ids = self.find_node_row(upper_node)
        low_node_ids = self.find_node_row(lower_node)
        self.shift_pos_down(low_node_ids)
        self.remove_row_col_neighbor_connections(up_node_ids, low_node_ids)
        self.add_col_row_between(up_node_ids, low_node_ids, col=False)

    def add_col_row_between(self, first, second, col=False):
        """!@brief Adds a column or row between already detached first and second row/column. 
        Does so by starting at top node and concatenating further nodes to their ancestor node (and vice versa). 
        All attributes except index are the average between first and second neighbor. 
        The first row/column needs to have the lower i/j value. 
        @param first The first row or column index (for inserting columns)
        @param second The second row or column index (for inserting rows)
        @param col Set to True if a column insertion is wanted."""
        if col:
            pos_shift = (0, 1)
        else:
            pos_shift = (1, 0)
        start_id = self.find_maxval_neuron("index", highest=True).index + 1
        # no need to append start node to prior node
        self.neurons.append(self.new_inter_neuron(
            start_id, first[0], second[0], pos_shift))
        self.make_neighbors(start_id, first[0])
        self.make_neighbors(start_id, second[0])
        for idx in range(1, len(first)):
            self.neurons.append(self.new_inter_neuron(start_id + idx,
                                                      first[idx], second[idx], pos_shift))
            self.make_neighbors(start_id + idx, first[idx])
            self.make_neighbors(start_id + idx, second[idx])
            self.make_neighbors(start_id + idx, start_id + idx - 1)

    def new_inter_neuron(self, new_id, conn1_id, conn2_id, pos_shift):
        """!@brief Calculates the properties of a new neuron and returns it, but will not adapt properties of surrounding neurons.
        The new neuron has properties weight, freq_activated and lerror as average of neurons with ids
        conn1 and conn2, no neighbors, and position based on conn1 neuron plus pos shift. 
        @remark Note that afterwards two neurons might have the same position.
        @param new_id The new neuron index of the new neuron
        @param conn1_id The index of the first reference neuron
        @param conn2_id The index of the first reference neuron
        @param pos_shift The position shift tuple which is added to the position of the neuron with index conn1_id
        @return A new neuron without neighbor relationships"""
        return Neuron(new_id, [],
                      add(self.neurons[conn1_id].weight,
                          self.neurons[conn2_id].weight)/2,
                      freq_activated=add(
            self.neurons[conn1_id].freq_activated, self.neurons[conn2_id].freq_activated)/2,
            lerror=add(self.neurons[conn1_id].lerror,
                       self.neurons[conn2_id].lerror)/2,
            position=add(self.neurons[conn1_id].pos, pos_shift),
            track_weights=self.track_neuron_weights,  epoch=self.epoch)

    # assumes that lefts and rights are sorted
    def remove_row_col_neighbor_connections(self, first, second):
        """!@brief Disattaches pairwise (index.sorted) neighbor connections between two lists of neurons
        @param first The first list of neurons IDs
        @param second The second list of neurons IDs
        @require The neurons meant to be disattached need to be at the same position in first and second list"""
        if len(first) != len(second):
            print("node lists to be separated do not have the same length")
            sys.exit()
        for idx in range(len(first)):
            self.neurons[first[idx]].neighbors.remove(second[idx])
            self.neurons[second[idx]].neighbors.remove(first[idx])

    def shift_pos_right(self, start_ids):
        """!@brief Shift the position of this column right and also for all columns right to this column beginning from a column with start_ids. 
        @param start_ids The neuron IDs of a column"""
        for index in start_ids:
            for r_id in self.find_right_nodes(index):
                self.neurons[r_id].pos[1] += 1
            self.neurons[index].pos[1] += 1

    def shift_pos_down(self, start_ids):
        """!@brief Shift the position of this row down and also for all columns below to this row beginning from a row with start_ids. 
        @param start_ids The neuron IDs of a row"""
        for index in start_ids:
            for d_id in self.find_lower_nodes(index):
                self.neurons[d_id].pos[0] += 1
            self.neurons[index].pos[0] += 1

    def find_right_nodes(self, curr):
        """!@brief Find all neurons right (in the same row) of a neuron
        @param curr The current neuron index
        @return A sorted list of neurons right of neuron curr"""
        res = []
        for n in self.neurons[curr].neighbors:
            if (self.neurons[int(n)].pos[0] == self.neurons[curr].pos[0]) and (self.neurons[int(n)].pos[1] > self.neurons[curr].pos[1]):
                res.append(int(n))
                res.extend(self.find_right_nodes(int(n)))
                break
        return res

    def find_left_nodes(self, curr):
        """!@brief Find all neurons left (in the same row) of a neuron
        @param curr The current neuron index
        @return A sorted list of neurons left of neuron curr"""
        res = []
        for n in self.neurons[curr].neighbors:
            if (self.neurons[int(n)].pos[0] == self.neurons[curr].pos[0]) and (self.neurons[int(n)].pos[1] < self.neurons[curr].pos[1]):
                res.extend(self.find_left_nodes(int(n)))
                res.append(int(n))
                break
        return res

    def find_node_column(self, n_id):
        """!@brief Finds the column in order from top to bottom that neuron with index n_id belongs to
        @param n_id The index of the neuron of the searched column
        @return The column"""
        column = []
        column.extend(self.find_upper_nodes(n_id))
        column.append(n_id)
        column.extend(self.find_lower_nodes(n_id))
        if len(column) != self.i:
            print("The found column had not the same length like the grid")
            print(("column length: " + str(self.i)))
            print(("found column length: " + str(len(column))))
            sys.exit()
        return column

    def find_node_row(self, n_id):
        """!@brief Finds the row in order from left to right that neuron with index n_id belongs to
        @param n_id The index of the neuron of the searched row
        @return The row"""
        row = []
        row.extend(self.find_left_nodes(n_id))
        row.append(n_id)
        row.extend(self.find_right_nodes(n_id))
        if len(row) != self.j:
            print("The found row had not the same length like the grid")
            print(("row length: " + str(self.j)))
            print(("found row length: " + str(len(row))))
            sys.exit()
        return row

    # need to find neurons by positions as we cannot use i and j for indexing

    def find_upper_nodes(self, curr):
        """!@brief Find all neurons above (in the same column) of a neuron
        @param curr The current neuron index
        @return A sorted list (top to bottom) of neurons above neuron curr"""
        res = []
        for n in self.neurons[curr].neighbors:
            if (self.neurons[int(n)].pos[1] == self.neurons[curr].pos[1]) and (self.neurons[int(n)].pos[0] < self.neurons[curr].pos[0]):
                res.extend(self.find_upper_nodes(int(n)))
                res.append(int(n))
                break
        return res

    def find_lower_nodes(self, curr):
        """!@brief Find all neurons below (in the same column) of a neuron
        @param curr The current neuron index
        @return a sorted list (top to bottom) of neurons below neuron curr"""
        res = []
        for n in self.neurons[curr].neighbors:
            if (self.neurons[int(n)].pos[1] == self.neurons[curr].pos[1]) and (self.neurons[int(n)].pos[0] > self.neurons[curr].pos[0]):
                res.append(int(n))
                res.extend(self.find_lower_nodes(int(n)))
                break
        return res

    def adapt_neurons(self, winner, input):
        """!@brief Update neuron attributes, but winner is treated before the entire neurons
        @param winner A winner neuron index
        @param input The input vector that the winner neuron has won"""
        self.update_winner(winner, input)
        self.update_all_neurons(winner, input)

    def update_winner(self, winner_index, input):
        """!@brief Update winner neuron attributes.
        @remark The winner weight is updated together with all other neurons
        @param winner A winner neuron index
        @param input The input vector that the winner neuron has won"""
        self.neurons[winner_index].freq_activated += 1

    def adaption(self):
        """!@brief Calculates the time dependent adaption factor based on time decay and the adaption factor alpha
        @return The time dependent adaption factor"""
        return self.alpha * self.decay()

    def update_all_neurons(self, winner, input):
        """!@brief performs neuron attribute updates for all neurons
        @param winner A winner neuron index
        @param input The input vector that the winner neuron has won"""
        self.update_all_weights(winner, input)

    def update_all_weights(self, winner, input):
        """!@brief Update all neuron weights
        @param winner A winner neuron index
        @param input The input vector that the winner neuron has won"""
        if not self.max_reached:
            adaption = self.alpha
        else:
            adaption = self.adaption()  # time dependent adaption
        for n in self.neurons:
            neighbor_degr = self.neighbour_degr(self.neurons[n.index].pos[0], self.neurons[n.index].pos[1],
                                                self.neurons[winner].pos[0], self.neurons[winner].pos[1])
            self.neurons[n.index].add_to_weight(adaption *
                                                neighbor_degr *
                                                subtract(input, n.weight))


##############################################################################################
##############################################################################################

# @param method may be dot or subtract (how to find the winner)
class NumpySOM(SimpleSOM):
    """!@brief A numpy vector based (not Neuron based) SOM model that keeps its grid topology constant as given initially. 
    Equals mostly the definitions of Self Organizing Map from Johnsson et al.: Haptic Perception with Self Organizing ANNs and an Anthropomorphic Hand.

    @param sigma The sigma for the computation of Gaussian neighborhood (default: 5).
    @param learning_rate The learning rate on how fast to adapt the weight vectors during training (default: 1)
    @param method Whether to use dot product or Euclidian distance for weight vector distance computation (default: "dot"). Choose any string except "dot" for euclidian distances. 
    @param num_features the number of classes for initialization purposes
    """

    def __init__(self, sigma=2, learning_rate=0.1, method="dot", num_classes=16,
                 **kwargs):
        self.w = list()
        super(NumpySOM, self).__init__(**kwargs)
        random.seed(12945)
        self.sigma = sigma
        self.method = method
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.init_label_map()
        self.init_model()
        self.repr_dict = {
            'model type': type(self),
            'grid size': self.initial_nodes,
            'max epochs': self.epochs,
            'init from input data': self.init_from_input,
            'with validation': self.validate,
            'debug mode': self.debug,
            'learning rate': self.learning_rate,
            'distance method': self.method,
            'sigma': sigma
        }

    def restore_model_defaults(self):
        """!@brief Restore all defaults specific to this model"""
        super(NumpySOM, self).restore_model_defaults()
        self.w = list()
        random.seed(12945)
        random.seed(12945)
        self.init_label_map()

    def adaption(self):
        """!@brief Calcualates an adaption factor based on time dependent decay and a give learning rate (see adaption definition by Johnsson et al. )
        @return The adaption factor"""
        return self.learning_rate * self.decay()

    def init_model(self):
        """!@brief Initializes the model"""
        self.w = zeros(list(self.initial_nodes) + [self.input_dim])
        for i in range(self.initial_nodes[0]):
            for j in range(self.initial_nodes[1]):
                self.w[i][j] = self.get_weight()

    def adapt_step(self, winner, input):
        """!@brief Adapt the model topology and neurons after processing one input vector.
        @param winner Index of the neuron which has won the matching for the  input vector
        @param input The input vector
        @return Any return values returned by adaption procedure"""
        self.update_nodes(winner, input)
        return True, ""

    def print_weights(self):
        """!@brief Print out all weight vectors of the model units rowwise"""
        for i in range(self.initial_nodes[0]):
            for j in range(self.initial_nodes[1]):
                print((str(i) + " " + str(j) + ": \n \t" + str(self.w[i][j])))

    def are_neighbors(self, u1, u2):
        """!@brief Calculate whether two units (aka tuple of coordinates) are neighbors. 
        @param u1 The first unit
        @param u2 The second unit
        @return Whether unit 1 and 2 are neighbors"""
        return sum(abs(subtract(u1, u2))) == 1

    def are_indirect_neighbors(self, u1, u2):
        """!@brief Calculate whether two units (aka tuple of coordinates) are indirect neighbors (share one neighbor). 
        @param u1 The first unit
        @param u2 The second unit
        @return whether unit 1 and 2 are indirect neighbors"""
        return sum(abs(subtract(u1, u2))) == 2

    def find_winner_neuron(self, input_vec):
        """!@brief Find the unit/neuron with weight vector most similiar to the input vector
        @remark In the case multiple units are equally matching best, one will be chosen randomly
        @param input_vec The input vector to compare to
        @retun The coordinate tuple of the best matching unit"""
        if self.method == "dot":
            inter = dot(self.w, input_vec)
            best_idx = where(inter == nanmax(inter))
        else:
            inter = npsum(absolute(subtract(self.w, input_vec)), axis=2)
            best_idx = where(inter == nanmin(inter))

        return (best_idx[0][0], best_idx[1][0])

    def find_second_winner_neuron(self, input_vec):
        """!@brief Find the unit/neuron with weight vector second most similiar to the input vector
        @remark In the case multiple units are equally matching best, one will be chosen randomly
        @param input_vec The input vector to compare to
        @retun The coordinate tuple of the second best matching unit"""
        if self.method == "dot":
            inter = dot(self.w, input_vec)
            best_idx = where(inter == nanmax(inter))
            inter[best_idx[0][0]][best_idx[1][0]] = min(array(inter).flatten())
            best_idx = where(inter == nanmax(inter))
        else:
            inter = npsum(absolute(subtract(self.w, input_vec)), axis=2)
            best_idx = where(inter == nanmin(inter))
            inter[best_idx[0][0]][best_idx[1][0]] = max(
                nparray(inter).flatten())
            best_idx = where(inter == nanmin(inter))
        return (best_idx[0][0], best_idx[1][0])

    def addtoweight(self, i, j, subs, winner):
        self.w[i][j] += self.adaption() * subs * \
            self.neighbour_degr(i, j, winner[0], winner[1])

    def update_nodes(self, winner, input_vec):
        subs = subtract(input_vec, self.w)
        _ = [self.addtoweight(i, j, subs[i][j], winner) for i, j in product(
            arange(self.initial_nodes[0]), arange(self.initial_nodes[1]))]

    def update_nodes_old(self, winner, input_vec):
        """!@brief Adapts all units (including winner) after receiving one input vector. 
        @param winner The coordinate tuple of the best matching unit
        @param input_vec The input vector that was won by the winner unit"""
        subs = subtract(input_vec, self.w)
        for i in range(self.initial_nodes[0]):
            for j in range(self.initial_nodes[1]):
                up = self.adaption() * subs[i][j] * \
                    self.neighbour_degr(i, j, winner[0], winner[1])
                #print("updating a weight by " + str(up))
                self.w[i][j] += up

    def init_label_map(self):
        """!@brief Initializes a label map (a label count grid) for all units with a given label count template. """
        self.label_map = dict()
        label_count_template = dict(
            zip(arange(self.num_classes), zeros(self.num_classes)))
        for i in range(self.initial_nodes[0]):
            for j in range(self.initial_nodes[1]):
                self.label_map[(i, j)] = label_count_template.copy()

    def update_label_map(self, winner, label):
        """!@brief Updates the label count map of the winner unit. 
        @param winner the winner unit which will be updated
        @param label the label of the input the winner unit has won"""
        self.label_map[winner][label] += 1

    def calc_winning_class_assignments(self):
        """!@brief Computes the winning class assignment map based on the label count maps of the units.
        @return the winning class assignment map mapping units to labels"""
        if self.label_map is None:
            raise RuntimeError(
                "The label map is not computed yet, please call <model>.label_map(args)")
        self.default_winner_label_counter = 0
        winning_class_assignments = {}
        for key in list(self.label_map.keys()):
            winning_class_assignments[key] = sorted(
                list(self.label_map[key].items()), key=lambda x: x[1], reverse=True)[0][0]
            # um(simplesom_5.label_map[(0,0)].values())
            if sum(self.label_map[key].values()) == 0:
                winning_class_assignments[key] = self.default_label
                self.default_winner_label_counter += 1
        default_prop = self.default_winner_label_counter / \
            prod((len(self.w), len(self.w[0])))*100
        self.default_props.append(default_prop)
        if self.debug:
            print("Set default label in", default_prop, "percent of cases.")
        self.winning_class_assignments = winning_class_assignments
        return winning_class_assignments

    def calc_default_label(self):
        """!@brief Calculates the default label and sets this as a model property"""
        if self.use_major_label_as_default:
            self.label_total = dict.fromkeys(self.distinct_labels, 0)
            if self.label_map is None:
                raise RuntimeError("The label map is not computed yet, \
                                    please call <model>.calc_label_map(args)")
            for i in range(self.initial_nodes[0]):
                for j in range(self.initial_nodes[1]):
                    for key in self.label_map[(i, j)]:
                        self.label_total[key] += self.label_map[(i, j)][key]
            self.default_label = sorted(list(self.label_total.items()),
                                        key=lambda x: x[1], reverse=True)[0][0]
        else:
            self.default_label = max(self.distinct_labels)+1

    def set_default_label(self, winner):
        """!@brief Sets the assigned label of a unit to the default label
        @remark Will throw an error if default label is not computed yet.
        @param Winner the coordinate tuple of a unit"""
        if self.default_label is None:
            raise RuntimeError("The default label is not computed yet, \
                                please call <model>.calc_default_label(args)")
        self.winning_class_assignments[winner] = self.default_label

    # gathers node ids and positions
    def gather_node_positions(self):
        """!@brief Creates a map of unit/node indices (rowwise enumerating) to coordinate tuples and saves this a model property
        @return The map from unit index to coordinate tuple"""
        res = {}
        for n in range(prod(self.initial_nodes)):
            res[n] = (floor(n/self.initial_nodes[1]), n %
                      self.initial_nodes[1])
        self.nd_pos_dict = res
        return res

    # these will return directed edges
    def calc_edges(self):
        """!@brief Creates a list of edges between neighbored units by regarding the SOM as a graph
        @return A list of edges"""
        edges = set()
        for n in range(prod(self.initial_nodes)):
            neighbors = self.get_network_neighbors(n)
            for ne in neighbors:
                edges.add(tuple(sorted([n, ne])))
        self.edges = list(edges)
        return list(edges)

    def get_winner_label_for_index(self, index):
        """!@brief Gets the winner label for unit index or coordinate tuple. 
        @param index A unit index or coordinate tuple"""
        if type(index) is tuple:
            return self.winning_class_assignments[index]
        else:
            return self.winning_class_assignments[(floor(index/self.initial_nodes[1]), index % self.initial_nodes[1])]

    def get_label_map_for_index(self, index):
        """!@brief Gets the label count map for unit index or coordinate tuple. 
        @param index A unit index or coordinate tuple"""
        if type(index) is tuple:
            return self.label_map[index]
        else:
            return self.label_map[(floor(index/self.initial_nodes[1]), index % self.initial_nodes[1])]

##############################################################################################
##############################################################################################


class GCS(GrowingSOM):
    """!@brief A class representing the Growing Cell Structure as defined by Johnsson et al..
    @details The most important characteristics of GCS are: 
   variable number of neurons
   only direct neighbors are updated
   adaption strength is constant over time
   beta (reduction percentage of local errors and freq_activated )
   alpha is inherited from GrowingSOM, it is the neighbor local error adaption factor 
   for en and ew (fraction how much of diff between winner/neighbor weight and input should affect the node weight)
   k-dimensional topology
    @param k the dimensionality of the topology (default: 2)
    @param l an integer representing the number of input vectors processed before the next growth or shrink step is performed (see lambda factor from Johnsson et al. )
    @param beta a decimal number. When all local neuron errors or freq_activated are reduced, they are reduced by beta*100\%. The freq_activated is reduced by beta_freq instead if it is given.
    @param beta_freq a decimal number. When all freq_activated are reduced, they are reduced by beta_freq*100\%.
    """

    def __init__(self,  k=2, beta=0.5,  beta_freq=None, random_new_weights=False, **kwargs):
        self.k = k
        self.beta = beta
        self.beta_freq = beta if beta_freq is None else beta_freq
        self.neurons = None
        self.nd_pos_dict = None
        self.edges = None  
        self.simplices = {}
        self.next_simplex = queue.Queue()
        self.random_new_weights = random_new_weights
        super(GCS, self).__init__(**kwargs)
        self.initial_nodes = None #ugly, but inherited from SimpleSOM
        self.repr_dict = {
            'model type': type(self),
            'k': self.k,
            'init from input data': self.init_from_input,
            'max size': self.max_size,
            'delete neurons': self.delete_neurons,
            'with validation': self.validate,
            'debug mode': self.debug,
            'max epochs': self.epochs,
            'lambda': self._lambda,
            'beta': self.beta,
            'beta_freq': self.beta_freq,
            'alpha (neighbor signal counter update rate)': self.alpha,
            'winner update factor': self.ew,
            'neighbor update factor': self.en,
            'number input vectors': len(self.inputs),
            'number validation vectors': len(self.X_vld)
        }

    def restore_model_defaults(self):
        """!@brief Restore all defaults specific to this model"""
        super(GCS, self).restore_model_defaults()
        self.neurons = None
        self.nd_pos_dict = None
        self.edges = None
        self.simplices = {}
        self.next_simplex = queue.Queue()


    def init_model(self):
        """!@brief Initializes the model. """
        neurons = []
        self.simplices[0] = []
        index = 0
        for index in range(self.k+1):
            # if index < (self.k + 1):
            neurons.append(Neuron(index, [i for i in range(self.k+1) if i != index], self.get_weight(), [
                           0], track_weights=self.track_neuron_weights, epoch=self.epoch))
            self.simplices[0].append(index)
         # only applicable if initialization with more than k+1 nodes must be supported
         #   else :
         #       neighbors = list(random.choice([i for i in range(index) if i != index], self.k, replace=False))
         #       neurons.append(Neuron(index, neighbors, weight, [index-self.k], track_weights=self.track_neuron_weights, epoch=self.epoch))
         #       self.simplices[index-self.k]=[index] + neighbors
         #       for neighb in neighbors:
         #           neurons[neighb].simplices.add(index-self.k)
         #           neurons[neighb].neighbors.add(index)
         #           for neighbneighb in neighbors:
         #               if neighb != neighbneighb:
         #                   neurons[neighb].neighbors.add(neighbneighb)
        self.next_simplex = queue.Queue()
        self.next_simplex.put(index-self.k+1)
        self.neurons = neurons
        if self.debug:
            print("initial nodes")
            for n in self.neurons:
                print((str(n.index)))
                print((str(n.neighbors)))
                print((str(n.simplices)))
                print(n.weight)
                print("-----------------------------")
            print("=================================")

    def __repr__(self):
        return str(self.repr_dict)

    def decrease_local_errors(self):
        """!@brief Decreases all neurons local errors by factor beta."""
        [i.decr_lerror(self.beta) for i in self.neurons]

    def calc_approx_voronoi(self, nid):
        """!@brief computes the size of the approximated Voronoi region of neuron at index nid
        @param nid the index of neuron
        @return the approximated size of the Voronoi region"""
        return sum([sum(abs(subtract(self.neurons[nid].weight, self.neurons[ne].weight))) for ne in self.neurons[nid].neighbors])/len(self.neurons[nid].neighbors)

    def decrease_freq_activated(self):
        """!@brief Decreases all neurons local errors by factor beta_freq."""
        [i.decr_freq(self.beta_freq) for i in self.neurons]

    def find_err_neuron(self):
        """!@brief Finds the neuron with the largest accumulated local error."""
        return self.find_maxval_neuron(attribute="lerror", highest=True)

    def find_freq_neuron(self):
        """!@brief Finds the neuron with the smallest relative signal counter aka frequency counter relative to the size of the neurons Voronoi region.
        Originally the local estimates were defined as the fraction of relative signal counter/size of Voronoi region. 
        As we only need a minimum and no absolute value of local probability estimate, I refrain from dividing the value through the sum of all neuron signal counters."""
        return argmin([n.freq_activated/self.calc_approx_voronoi(n.index) for n in self.neurons])

    def find_delete_neuron(self):
        """!@brief Finds the neuron with the minimum relative signal counter aka frequency counter.
        @return The index of the neuron to be deleted according to model criteria. """
        return self.find_freq_neuron()

    def grow_step(self, winner, input):
        """!@brief Initiates one grow step by calling proper model modification methods. 
        @param winner A winner neuron index
        @param input The input vector this neuron has won
        @return The output of called model modification methods. """
        return self.insert_neuron()

    def check_growable(self):
        """!@brief Checks if the model can still grow. This is the case when the model owns simplices. 
        @return Whether the model can still grow. """
        return (len(self.simplices) > 0)

    def shrink_step(self):
        """!@brief Initiates one model shrink step by calling proper model modification methods. 
        @return The output of called model modification methods. """
        return self.delete_neuron()

    def update_all_neurons(self):
        """!@brief Updates neuron attributes. Usually executed after each input vector processing. """
        self.decrease_freq_activated()

    def update_winner(self, winner_index, input):
        """!@brief Updates the winner neuron attributes. 
        @param winner_index A winner neuron index
        @param input The input vector this neuron has won"""
        self.update_winner_weight(winner_index, input)
        self.update_winner_lerror(winner_index, input)
        # only for after training evaluation
        self.neurons[winner_index].freq_activated += 1

    def update_winner_lerror(self, winner, input):
        """!@brief Increases the winner local error by the squared euclidian distance between weight and input.
        @param winner A winner neuron index
        @param input The input vector this neuron has won"""
        self.neurons[winner].lerror += sum(
            abs(subtract(self.neurons[winner].weight, input)))

    def update_after_deletion(self, to_delete):
        """!@brief Updates model  according to the neuron which is to be deleted. 
        The update takes into account neuron indices (all neurons which have a larger index that to_delete get a decreased index).
        This also applies to neuron neighbor lists and simplices sets, here all IDs smaller than to_delete are decreased by 1. 
        @param to_delete The index of the neuron to be deleted
        """
        for n in self.neurons:
            if n.index > to_delete:
                # indices shift left, but not ids yet
                self.neurons[n.index-1].index -= 1
            self.neurons[n.index].neighbors = set(
                decrease_if_bigger(self.neurons[n.index].neighbors, to_delete))
        for s in list(self.simplices.keys()):
            self.simplices[s] = decrease_if_bigger(
                self.simplices[s], to_delete)

    def delete_neuron(self):
        """!@brief Deletes one neuron which has the minimum local signal counter relative to its voronoi region size. 
        Adapts the model accordingly, meaning incomplete simplices are removed and any neurons without any connections are also removed. """
        to_delete = self.find_delete_neuron()
        if self.verbose:
            print("------------------")
            print("Neuron to remove: ", to_delete)
            print(("Simplices to remove: " +
                   str(self.neurons[to_delete].simplices)))
        ########
        for n in self.neurons[to_delete].neighbors:
            self.neurons[n].neighbors.discard(to_delete)
            self.neurons[n].simplices = self.neurons[n].simplices.difference(
                self.neurons[to_delete].simplices)
            # after all simplices have been removed from nodes,
            # check if some neighbors of to_delete are connected with each other without sharing a simplex
            # first need to gather all these edges cause iterables cannot change size during iteration
        for s in self.neurons[to_delete].simplices:
            edges_to_delete = []
            for n in self.simplices[s]:
                if n != to_delete:
                    for ne in [nei for nei in self.neurons[n].neighbors if nei != to_delete]:
                        if len(self.neurons[n].simplices.intersection(self.neurons[ne].simplices)) == 0:
                            edges_to_delete.append(sorted([n, ne]))
            for e in edges_to_delete:
                self.neurons[e[0]].neighbors.discard(e[1])
                self.neurons[e[1]].neighbors.discard(e[0])
            self.simplices.pop(s)
            self.next_simplex.put(s)

        # when a neighbor has no simplices it belongs to, delete it
        neighbor_qu = queue.Queue()
        list(map(neighbor_qu.put, sorted(self.neurons[to_delete].neighbors)))
        while not neighbor_qu.empty():
            n = neighbor_qu.get()
            if not self.neurons[n].simplices or not self.neurons[n].neighbors:
                for ne in self.neurons[n].neighbors:
                    self.neurons[ne].neighbors.discard(n)
                self.neurons.pop(n)
                if self.verbose:
                    print("deleted neuron ", n)
                self.update_after_deletion(n)
                if n < to_delete:
                    to_delete -= 1
                neighbor_qu = queue.Queue()
                list(map(neighbor_qu.put, sorted(
                    self.neurons[to_delete].neighbors)))
        # delete the neuron itself
        self.neurons.pop(to_delete)
        self.update_after_deletion(to_delete)


    def insert_neuron(self):
        """!@brief Inserts a neuron."""
        if len(self.neurons) == 0:
            print("Model has no neurons! ")
            print(self)
            print(self.neurons)
        q = self.find_err_neuron().index
        f = self.find_maxweightdiff_neighbor(q)
        affected_simplices = self.neurons[f].simplices.intersection(
            self.neurons[q].simplices)
        if self.debug:
            print("affected simplices by insertion: \n", affected_simplices)
        if len(affected_simplices) == 0:
            print("ERROR: could not find a simplex between nodes while adding new node. There must be a bug in the implementation.")
            print(("q: " + str(self.neurons[q])))
            print(("f: " + str(self.neurons[f])))
            for n in self.neurons:
                print((str(n.index)))
                print((str(n.neighbors)))
                print((str(n.simplices)))
                print("-----------------------------")
            print("=================================")
            raise RuntimeError
        # new neuron r
        r_neighbors = set(
            [q, f] + list(self.neurons[q].neighbors.intersection(self.neurons[f].neighbors)))
        if self.random_new_weights:
            r_weight = self.pull_random_weight()
        else:
            r_weight = add(self.neurons[q].weight, self.neurons[f].weight)/2
        if self.debug:
            print("New neuron weight: ", r_weight)
            print("Parents: ", self.neurons[q].weight, self.neurons[f].weight)
        r = Neuron(len(self.neurons), r_neighbors, r_weight, [],
                   track_weights=self.track_neuron_weights, epoch=self.epoch)
        self.neurons.append(r)
        for rn in r_neighbors:
            approx_voronoi_before = self.calc_approx_voronoi(rn)
            if rn == f:
                self.neurons[f].neighbors.discard(q)
            if rn == q:
                self.neurons[q].neighbors.discard(f)
            self.neurons[rn].add_neighbor(r.index)
            self.neurons[rn].decr_lerror(self.alpha, len(r_neighbors))
            # signal counter aka freq_activated adaption
            delta_freq = ((self.calc_approx_voronoi(rn)-approx_voronoi_before) /
                          approx_voronoi_before)*self.neurons[rn].freq_activated
            self.neurons[rn].freq_activated += delta_freq
            r.freq_activated += abs(delta_freq)
        r.lerror = sum(
            [n.lerror for n in self.neurons if n.index in r_neighbors])/len(r_neighbors)
        self.decrease_local_errors()
        if self.debug:
            print(("Added neuron at position " + str(r.index)))
        # update simplices that contain f and q and divide them

        for s in affected_simplices:
            if self.next_simplex.empty():
                self.next_simplex.put(max(self.simplices.keys()) + 1)
            new_s = self.next_simplex.get()
            # new_s contains q and r and one other neighbor. Update simplex list of q.
            self.simplices[new_s] = list(
                [i for i in self.simplices[s] if i != self.neurons[f].index] + [r.index])
            self.neurons[q].simplices.add(new_s)
            self.neurons[q].simplices.discard(s)
            # s contains f, r and one other neighbor
            self.simplices[s] = [i for i in self.simplices[s]
                                 if i != self.neurons[q].index] + [r.index]
            # s is included in f simplices anyway
            self.neurons[r.index].simplices.add(s)
            for node in list(set(self.simplices[s]) & set(self.simplices[new_s])):
                self.neurons[node].simplices.add(new_s)
        for n in self.neurons:
            if len(self.neurons[n.index].simplices) == 0:
                print("no simplex is assigned to the following neuron: \n",
                      self.neurons[n.index])
                raise RuntimeWarning
        return True, ""
