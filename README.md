# Self Organizing Artifical Neural Networks

This repo contains the main python module which was part of my bachelor thesis experiments. The original thesis topic is "Haptic Object Classification with Self Organizing Neural Networks". Here, only the implementation of the self-organizing network algorithms can be found. 

Attached you find the class diagram. The shell script for generating the script and documentation of the classes are also provided. 
The implemented networks are variants of the Self-Organizing Map. 

## Implemented Networks

### Growing Grid

If you are familiar with the Self-Organizing Map (=SOM) as first published by Teuvo Kohonen (here is the [publication](https://ieeexplore.ieee.org/abstract/document/58325?casa_token=RbwvmWzmxAIAAAAA:gcEV86W5BkoQXOkdKeoKPjejd0vyWe8EqZ7vFosyDGtwpb-HeYQsakDT4I9KNXGqHeyTkTHRgw)), the idea is very easy: Instead of setting the grid size and connection topology prior to training, these network properties are adapted during the training. The Growing Grid was published by Bernd Fritzke, here is the [google scholar link](https://scholar.google.de/citations?view_op=view_citation&hl=en&user=eIKJKxAAAAAJ&citation_for_view=eIKJKxAAAAAJ:d1gkVwhDpl0C). 

### GCS (Growing Cell Structure)

A growing network for neural network published by Bernd Fritzke. Here is the [google scholar link](https://www.sciencedirect.com/science/article/abs/pii/0893608094900914). 

In short: These networks fulfill the property that each node is at any time member of a simplex (fully connected subgraph) of size k. In my work I have mainly experimented with simplices in very simple shapes, or let's simply call them triangles and hypertetrahedons. Another important parameter is whether deletion of "unimportant" neurons is allowed. 

**I publish my work so you can contribute and benefit of my work done. Feel free to fork it or hand in MRs. Thank you!**






